# CBLab web site source code

It is based on Petridish, which is a Jekyll theme for research project websites [https://github.com/peterdesmet/petridish](https://github.com/peterdesmet/petridish).

## Carpetes i fitxers

  * *_data*:  fitxers format YML amb informació sobre items. El *team.yml* té les dades de la pagina Group Members
  * *_posts*: fitxers pel blog/news
  * *_bibliography*: el fitxer bibtex
  * *assets*: directori que es copiarà tal qual a la web final. Conté imatges, i altres (css, js ) necessaris per la web final.
  * *pages*: fitxers que tenen els continguts de les diferents planes 
  * *_layouts*: fitxers en HTML que formategen la informació per algunes planes
  * *_includes*: altre codi HTML necessari 
  * *_sass*: fitxers en codi css
  * *_pluggins*: configuracio jekill
  * *_site*: aqui es genera el site web final

