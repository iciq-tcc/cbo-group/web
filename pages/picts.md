---
layout: default
title: Pictures
description: Some computer  synthesized images
background: /assets/img/IMG_5749.jpeg
permalink: /picts/
galleries:
  - title: Link to homepage
    image: /assets/picts/templage_original.jpg
    url: /
  - title: Link to image gallery
    image: /uploads/album/2.jpg
    url: /assets/picts/image015.jpg
---

{% include image-gallery.html folder="/assets/picts" %}
