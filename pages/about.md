---
layout: home
title: About
description: 
background: /assets/img/IMG_5749.jpeg
permalink: /about/
---
#### The lab was founded in March 2004.This project is aimed at applying modern computational chemistry methods to solve problems in the fields of catalysis and nanoscience, some of those called emerging chemical technologies that show great promise for alleviating important societal problems. Basically, this research program deals with two kinds of topics. On the one hand, the understanding and  “in-silico” design of catalysts for chemical process that are crucial for solving climate change effects and energy related issues. On the other hand, the computational treatment of molecular metal-oxides towards new technological materials. 
###### @HartreeFoca, our mascot, arrived in the Lab on June 20 2020, under COVID-19 mobility restrictions. She is having fun in [Instagram](https://www.instagram.com/hartreefoca/) and [Twitter](https://twitter.com/hartreefoca).   

![alt text]({{ site.baseurl }}/assets/img/IMG_5539.jpeg)

###### We'll never forget that memorable moment:

<iframe width="560" height="315" src="{{ site.baseurl }}/assets/img/foca_arrival.mp4" 
frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"  allowfullscreen></iframe>


