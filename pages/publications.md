---
Layout: bibliography
title: Publications
background: /assets/img/IMG_5749.jpeg
permalink: /publications/
excerpt: aaa
---
#### Articles
{% bibliography --query @article %}
#### Book Chapters
{% bibliography --query @book %}
{% bibliography --query @incollection %}
