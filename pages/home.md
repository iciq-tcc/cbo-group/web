---
layout: home
title: CompChem 4.0
description: Computational Chemistry  for a Sustainable Development
background: /assets/img/IMG_5749.jpeg
permalink: /
---
#### In this laboratory, hosted by the [Institute of Chemical Research of Catalonia (ICIQ)](https://www.iciq.org/), we appy computational chemistry methods to provide solutions and new ideas in the fields of catalysis,  supramolecular chemistry and molecular metal oxides. We love #opendata and creating #opensource [tools](/downloads) for general use.


![alt text]({{ site.baseurl }}/assets/img/GrupBO_dec19.jpg) _Group picture December 2019_

![alt text]({{ site.baseurl }}/assets/img/5-6-2020.jpg)_Group picture June 2020_

![alt text]({{ site.baseurl }}/assets/img/GrupBO_feb21.jpg)_Group picture February 2021_
